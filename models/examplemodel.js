// Example Model

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ExampleModel = new Schema({
	name: { type: String, required: true },
	slug: { type: String, required: true },
	body: { type: String, required: true }}, {
	collection: 'example'
});

module.exports = mongoose.model('ExampleModel', ExampleModel);