// Example Node App

var express = require('express');
var app = express();
var http = require('http');

var options = require('./init/config');

require('./init/environment')(app, express, options.environment);
require('./init/routes')(app, options.routes);
require('./init/database')(options.database);

http.createServer(app).listen(options.general.port);

console.log('node-example listening on port: ' + options.general.port);