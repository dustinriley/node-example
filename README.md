# node-example

Basic node.js application using Express and Mongoose.

## LICENSE

node-example is distributed under the terms of the CC0 1.0 Universal license. See COPYING for details.
