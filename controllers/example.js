// Example Controller

var exampleModel = require('../models/examplemodel');

exports.index = function(req, res) {
	exampleModel.find(function(err, examples) {
		res.send(examples);
	});
}

exports.getBySlug = function(req, res) {
	exampleModel.findOne({slug: req.params.slug}, function(err, example) {
		res.send({example: example});
	});
}