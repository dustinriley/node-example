// Application Configuration

module.exports = {
	general: {
		port: 3000
	},
	environment: {

	},
	routes: {

	},
	database: {
		host: '127.0.0.1',
		port: 27017,
		database: 'exampledb',
		user: '',
		password: ''
	}
};