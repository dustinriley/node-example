// Route Definitions

var example = require('../controllers/example');

module.exports = function(app, options) {
	// Example Controller Routes
	app.get('/example', example.index);
	app.get('/example/:slug', example.getBySlug);
}