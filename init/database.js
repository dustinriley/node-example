// Database Connection

var mongoose = require('mongoose');

module.exports = function(options) {
	var connString = 'mongodb://';

	if (options.user != '') {
		conString += options.user + ':' + options.password + '@';
	}

	connString += options.host + ':' + options.port + '/' + options.database;

	mongoose.connect(connString, function(err) {
		if (err) {
			console.log('database connection error: ' + err);
			process.exit(0);
		}
	});

	mongoose.connection.on('open', function() {
		console.log('connected to database');
	});

	mongoose.connection.on('close', function() {
		console.log('disconnected from database');
	});
}