// Environment Settings

module.exports = function(app, express, options) {
	app.configure(function() {
		app.use(express.bodyParser());
		app.use(express.methodOverride());
		app.use(app.router);
	});

	app.configure('development', function() {
		app.use(express.errorHandler({
			dumpExceptions: true,
			showStack: true
		}));
	});

	app.configure('production', function() {
		app.use(express.errorHandler());
	});
}